<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{

    /**
     * @Route(path="/", methods={"GET"}, name="static_main_page_action")
     */
    public function mainPageAction()
    {
        return $this->render('base.html.twig');
    }
}