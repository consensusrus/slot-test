<?php

namespace App\Controller\Account;

use App\Entity\Gift\UserGift;
use App\Entity\User;
use App\Service\Gift\GiftRaffleService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserAccountController
 * @package App\Controller\Account
 *
 * @IsGranted("ROLE_USER")
 */
class UserAccountController extends AbstractController
{

    /**
     * @param Request                $request
     *
     * @param GiftRaffleService      $giftRaffleService
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     *
     * @Route(path="/user/cabinet", methods={"GET", "POST"}, name="static_user_cabinet_action")
     */

    public function cabinetPageAction(Request $request, GiftRaffleService $giftRaffleService, EntityManagerInterface $entityManager)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($request->isMethod('POST')) {
            $gift = $giftRaffleService->getRandomGiftForUser($currentUser);

            $entityManager->persist($gift);
            $entityManager->flush();
        }

        $gifts = $entityManager->getRepository(UserGift::class)->findBy([
            'user' => $currentUser
        ]);

        return $this->render('cabinet/index.html.twig', [ 'gifts' => $gifts ]);
    }

}