<?php

namespace App\Command\Gift;

use App\Entity\User;
use App\Service\Gift\GiftRaffleService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GiftRaffleCommand extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var GiftRaffleService
     */
    private $giftRaffleService;


    public function __construct(EntityManagerInterface $entityManager, GiftRaffleService $giftRaffleService, string $name = null)
    {
        parent::__construct($name);

        $this->entityManager     = $entityManager;
        $this->giftRaffleService = $giftRaffleService;
    }


    protected static $defaultName = 'gift:raffle';


    protected function configure()
    {
        $this
            ->addArgument('batchSize', InputArgument::OPTIONAL, 'Укажите шаг для обработки', 20);
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var User[] $usersWithoutGifts */
        $usersWithoutGifts = $this->entityManager->getRepository(User::class)->findUsersWithoutGifts();

        $batchSize = (int)$input->getArgument('batchSize');

        $output->writeln('Указан шаг: ' . $batchSize . PHP_EOL);

        ProgressBar::setFormatDefinition('custom', '%current%/%max% [%bar%] %percent:3s%% [%message%]');
        $progressBar = new ProgressBar($output, count($usersWithoutGifts));
        $progressBar->setFormat('custom');

        foreach ($usersWithoutGifts as $index => $user) {
            $user = $this->entityManager->find(User::class, $user->getId());

            $progressBar->setMessage($user->getEmail());

            $gift = $this->giftRaffleService->getRandomGiftForUser($user);

            $this->entityManager->persist($gift);

            if ($index !== 0 && ($index % $batchSize) === 0) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }

            $progressBar->advance();
        }

        $output->writeln('DONE');

        return 1;
    }
}