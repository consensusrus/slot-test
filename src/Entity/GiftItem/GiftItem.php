<?php

namespace App\Entity\GiftItem;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class GiftItem
 * @package App\Entity\GiftItem
 *
 * @ORM\Entity()
 * @ORM\Table(name="gifts_items")
 */
class GiftItem
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }


    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
}