<?php

namespace App\Entity\Gift;

use App\Entity\Gift\Value\MoneyGiftValue;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MoneyGift
 * @package App\Entity\Gift
 *
 * @ORM\Entity()
 */
class MoneyGift extends UserGift
{

    /**
     * @var MoneyGiftValue|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\Value\MoneyGiftValue", mappedBy="gift", cascade={"persist", "merge"})
     */
    protected $value;


    public function __construct()
    {
        parent::__construct();

        $this->setValue(new MoneyGiftValue());
    }


    public function getType()
    {
        return 'money';
    }


    public function getTextValue()
    {
        return $this->getValue() ? $this->getValue()->getValue() : null;
    }


    public function getHumanType()
    {
        return 'Деньги';
    }
}