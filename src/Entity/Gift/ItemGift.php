<?php

namespace App\Entity\Gift;

use App\Entity\Gift\Value\ItemGiftValue;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ItemGift
 * @package App\Entity\Gift
 *
 * @ORM\Entity()
 */
class ItemGift extends UserGift
{

    /**
     * @var ItemGiftValue|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\Value\ItemGiftValue", mappedBy="gift", cascade={"persist", "merge"})
     */
    protected $value;


    public function __construct()
    {
        parent::__construct();

        $this->setValue(new ItemGiftValue());
    }


    public function getType()
    {
        return 'item';
    }


    /**
     * @return ItemGiftValue|null
     */
    public function getValue()
    {
        return $this->value;
    }


    public function getTextValue()
    {
        return $this->getValue() && $this->getValue()->getValue() ? $this->getValue()->getValue()->getTitle() : null;
    }


    public function getHumanType()
    {
        return 'Вещи';
    }
}