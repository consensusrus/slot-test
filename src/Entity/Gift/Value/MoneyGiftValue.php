<?php

namespace App\Entity\Gift\Value;

use App\Entity\Gift\MoneyGift;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MoneyGiftValue
 * @package App\Entity\Gift\Value
 *
 * @ORM\Entity()
 * @ORM\Table(name="gifts_money_values")
 */
class MoneyGiftValue extends UserGiftValue
{

    /**
     * @var null|float
     *
     * @ORM\Column(type="float", nullable=false)
     */
    protected $value;

    /**
     * @var MoneyGift|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\MoneyGift", inversedBy="value")
     * @ORM\JoinColumn(name="gift_id", referencedColumnName="id")
     */
    protected $gift;


    public function __construct()
    {
        $value = mt_rand(1, 20000) / mt_getrandmax();

        $this->setValue($value);
    }


    /**
     * @return null|float
     */
    public function getValue(): ?float
    {
        return $this->value;
    }


    /**
     * @param null|float $value
     */
    public function setValue(?float $value): void
    {
        $this->value = round($value, 2);
    }


    /**
     * @param MoneyGift|null $gift
     */
    public function setGift($gift): void
    {
        $this->gift = $gift;
    }


    /**
     * @return MoneyGift|null
     */
    public function getGift(): ?MoneyGift
    {
        return $this->gift;
    }
}