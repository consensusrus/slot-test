<?php

namespace App\Entity\Gift\Value;

use App\Entity\Gift\ItemGift;
use App\Entity\GiftItem\GiftItem;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ItemGiftValue
 * @package App\Entity\Gift\Value
 *
 * @ORM\Entity()
 * @ORM\Table(name="gifts_item_values")
 */
class ItemGiftValue extends UserGiftValue
{

    /**
     * @var GiftItem|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\GiftItem\GiftItem")
     * @ORM\JoinColumn(name="gift_item_id", referencedColumnName="id")
     */
    protected $value;

    /**
     * @var ItemGift|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\ItemGift", inversedBy="value")
     * @ORM\JoinColumn(name="gift_id", referencedColumnName="id")
     */
    protected $gift;


    /**
     * @return GiftItem|null
     */
    public function getValue(): ?GiftItem
    {
        return $this->value;
    }


    /**
     * @param GiftItem|null $value
     */
    public function setValue(?GiftItem $value): void
    {
        $this->value = $value;
    }


    /**
     * @return ItemGift|null
     */
    public function getGift(): ?ItemGift
    {
        return $this->gift;
    }


    /**
     * @param ItemGift|null $gift
     */
    public function setGift($gift): void
    {
        $this->gift = $gift;
    }

}