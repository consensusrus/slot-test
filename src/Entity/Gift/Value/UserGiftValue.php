<?php

namespace App\Entity\Gift\Value;

use App\Entity\Gift\UserGift;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass()
 */
abstract class UserGiftValue
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var UserGift
     */
    protected $gift;

    protected $value;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    abstract public function getGift();


    abstract public function setGift($gift);
}