<?php

namespace App\Entity\Gift\Value;

use App\Entity\Gift\BonusGift;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BonusGiftValue
 * @package App\Entity\Gift\Value
 *
 * @ORM\Entity()
 * @ORM\Table(name="gifts_bonus_values")
 */
class BonusGiftValue extends UserGiftValue
{

    /**
     * @var null|integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $value;

    /**
     * @var BonusGift|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\BonusGift", inversedBy="value")
     * @ORM\JoinColumn(name="gift_id", referencedColumnName="id")
     */
    protected $gift;


    public function __construct()
    {
        $this->value = mt_rand(1, 20000);
    }


    /**
     * @return BonusGift|null
     */
    public function getGift(): ?BonusGift
    {
        return $this->gift;
    }


    /**
     * @param BonusGift|null $gift
     */
    public function setGift($gift): void
    {
        $this->gift = $gift;
    }


    /**
     * @return null|integer
     */
    public function getValue(): ?int
    {
        return $this->value;
    }


    /**
     * @param null|integer $value
     */
    public function setValue(?int $value): void
    {
        $this->value = $value;
    }
}