<?php

namespace App\Entity\Gift;

use App\Entity\Gift\Value\BonusGiftValue;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BonusGift
 * @package App\Entity\Gift
 *
 * @ORM\Entity()
 */
class BonusGift extends UserGift
{

    /**
     * @var BonusGiftValue|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Gift\Value\BonusGiftValue", mappedBy="gift", cascade={"persist", "merge"})
     */
    protected $value;


    public function __construct()
    {
        parent::__construct();

        $this->setValue(new BonusGiftValue());
    }


    public function getType()
    {
        return 'bonus';
    }


    public function getTextValue()
    {
        return $this->getValue() ? $this->getValue()->getValue() : null;
    }


    /**
     * @return BonusGiftValue|null
     */
    public function getValue(): ?BonusGiftValue
    {
        return $this->value;
    }


    public function getHumanType()
    {
        return 'Бонусы';
    }
}