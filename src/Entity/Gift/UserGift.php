<?php

namespace App\Entity\Gift;

use App\Entity\Gift\Value\UserGiftValue;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserGift
 * @package App\Entity\Gift
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="gifts")
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "bonus" = "App\Entity\Gift\BonusGift",
 *     "item" = "App\Entity\Gift\ItemGift",
 *     "money" = "App\Entity\Gift\MoneyGift"
 * })
 */
abstract class UserGift
{

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="gifts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var UserGiftValue
     */
    protected $value;


    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }


    public function getDateString()
    {
        if ($this->timestamp instanceof \DateTimeInterface) {
            return date_format($this->timestamp, 'd.m.Y');
        } else {
            return $this->timestamp;
        }
    }


    /**
     * @return UserGiftValue
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * @param UserGiftValue $value
     */
    public function setValue($value): void
    {
        $value->setGift($this);

        $this->value = $value;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return \DateTime|null
     */
    public function getTimestamp(): ?\DateTime
    {
        return $this->timestamp;
    }


    /**
     * @param \DateTime|null $timestamp
     */
    public function setTimestamp(?\DateTime $timestamp): void
    {
        $this->timestamp = $timestamp;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }


    abstract public function getTextValue();


    abstract public function getHumanType();


    abstract public function getType();

}