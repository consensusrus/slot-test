<?php

namespace App\Service\Gift;

use App\Entity\Gift\UserGift;
use App\Service\Gift\Handlers\HandlerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class GiftRaffleHandlersManager
{

    /** @var HandlerInterface[] */
    private $handlers;


    public function __construct()
    {
        $this->handlers = new ArrayCollection();
    }


    public function getHandlers()
    {
        return $this->handlers;
    }


    public function getHandlerForGift(UserGift $gift): ?HandlerInterface
    {
        foreach ($this->getHandlers() as $handler) {
            if ($handler->support($gift)) {
                return $handler;
            }
        }

        return null;
    }


    public function getAllowedGiftClasses()
    {
        $handlerClasses = [];

        foreach ($this->getHandlers() as $handler) {
            $handlerClasses[] = $handler->getClass();
        }

        return $handlerClasses;
    }


    public function addHandler(HandlerInterface $handler)
    {
        if ( ! $this->handlers->contains($handler)) {
            $this->handlers->add($handler);
        }
    }
}