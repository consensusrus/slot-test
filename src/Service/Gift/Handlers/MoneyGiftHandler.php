<?php

namespace App\Service\Gift\Handlers;

use App\Entity\Gift\MoneyGift;
use App\Entity\Gift\UserGift;
use Doctrine\ORM\EntityManagerInterface;

class MoneyGiftHandler implements HandlerInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function handle(UserGift $gift): UserGift
    {
        /** @var MoneyGift $moneyGift */
        $moneyGift = $gift;

        $value = mt_rand(1, 20000);

        $moneyGift->getValue()->setValue($value);

        return $gift;
    }


    public function support(UserGift $gift): bool
    {
        return $gift instanceof MoneyGift;
    }


    public function getClass(): string
    {
        return MoneyGift::class;
    }
}