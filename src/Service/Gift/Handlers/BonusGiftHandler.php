<?php

namespace App\Service\Gift\Handlers;

use App\Entity\Gift\BonusGift;
use App\Entity\Gift\UserGift;
use Doctrine\ORM\EntityManagerInterface;

class BonusGiftHandler implements HandlerInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function handle(UserGift $gift): UserGift
    {
        /** @var BonusGift $bonusGift */
        $bonusGift = $gift;

        $bonusGift->getValue()->setValue(rand(1, 20000));

        return $gift;
    }


    public function support(UserGift $gift): bool
    {
        return $gift instanceof BonusGift;
    }

    public function getClass(): string
    {
        return BonusGift::class;
    }
}