<?php

namespace App\Service\Gift\Handlers;

use App\Entity\Gift\UserGift;

interface HandlerInterface
{

    public function handle(UserGift $gift): UserGift;


    public function getClass(): string;


    public function support(UserGift $gift): bool;
}