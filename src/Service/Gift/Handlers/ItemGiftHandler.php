<?php

namespace App\Service\Gift\Handlers;

use App\Entity\Gift\ItemGift;
use App\Entity\Gift\UserGift;
use App\Entity\GiftItem\GiftItem;
use Doctrine\ORM\EntityManagerInterface;

class ItemGiftHandler implements HandlerInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function handle(UserGift $gift): UserGift
    {
        try {
            /** @var ItemGift $itemGift */
            $itemGift = $gift;

            $allowedGiftItems = $this->entityManager->getRepository(GiftItem::class)->findAll();

            $randIndex = mt_rand(0, count($allowedGiftItems) - 1);

            /** @var GiftItem $randItem */
            $randItem = $allowedGiftItems[$randIndex];

            $itemGift->getValue()->setValue($randItem);
        } catch (\Throwable $exception) {
        }

        return $gift;
    }


    public function support(UserGift $gift): bool
    {
        return $gift instanceof ItemGift;
    }


    public function getClass(): string
    {
        return ItemGift::class;
    }
}