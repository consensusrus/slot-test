<?php

namespace App\Service\Gift;

use App\Entity\Gift\BonusGift;
use App\Entity\Gift\MoneyGift;
use App\Service\Gift\Factory\GiftFactory;

class BonusGiftConvertService
{

    public const COEFFICIENT = 2.2;


    /**
     * @param MoneyGift $moneyGift
     *
     * @return BonusGift
     */
    public function convertMoneyToBonus(MoneyGift $moneyGift): BonusGift
    {
        $moneySum = $moneyGift->getTextValue();

        /** @var BonusGift $bonusGift */
        $bonusGift = GiftFactory::makeGift(GiftFactory::GIFT_BONUS);

        $bonuses = round($moneySum * self::COEFFICIENT, 1);

        $bonusGift->getValue()->setValue($bonuses);

        return $bonusGift;
    }
}