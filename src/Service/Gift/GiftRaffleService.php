<?php

namespace App\Service\Gift;

use App\Entity\Gift\UserGift;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class GiftRaffleService
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var GiftRaffleHandlersManager
     */
    private $giftRaffleHandlersManager;


    public function __construct(EntityManagerInterface $entityManager, GiftRaffleHandlersManager $giftRaffleHandlersManager)
    {

        $this->entityManager             = $entityManager;
        $this->giftRaffleHandlersManager = $giftRaffleHandlersManager;
    }


    public function getRandomGiftForUser(User $user): ?UserGift
    {
        $allowedClasses = $this->giftRaffleHandlersManager->getAllowedGiftClasses();

        if (empty($allowedClasses)) {
            return null;
        }

        /** @var UserGift $randomGift */
        $randomGiftClass = $this->getRandomClass($allowedClasses);

        $randomGift = new $randomGiftClass;

        $giftHandler = $this->giftRaffleHandlersManager->getHandlerForGift($randomGift);

        $giftHandler->handle($randomGift);
        $randomGift->setUser($user);

        return $randomGift;
    }


    protected function getRandomClass($allowedClasses)
    {
        $randomIndex = mt_rand(0, count($allowedClasses) - 1);

        return $allowedClasses[$randomIndex];
    }
}