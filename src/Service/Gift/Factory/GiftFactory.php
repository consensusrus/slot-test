<?php

namespace App\Service\Gift\Factory;

use App\Service\Gift\Factory\Generators\BonusGiftGenerator;
use App\Service\Gift\Factory\Generators\ItemGiftGenerator;
use App\Service\Gift\Factory\Generators\MoneyGiftGenerator;

class GiftFactory
{

    public const GIFT_BONUS = 'bonus';

    public const GIFT_MONEY = 'money';

    public const GIFT_ITEM = 'item';

    protected static $generatorsMapping = [
        'bonus' => BonusGiftGenerator::class,
        'money' => MoneyGiftGenerator::class,
        'item'  => ItemGiftGenerator::class
    ];


    public static function makeGift($giftType)
    {
        if (false === isset(self::$generatorsMapping[$giftType])) {
            throw new \LogicException('Unknown gift type');
        }

        return (new self::$generatorsMapping[$giftType])->generate();
    }
}