<?php

namespace App\Service\Gift\Factory\Generators;

use App\Entity\Gift\MoneyGift;
use App\Entity\Gift\UserGift;

class MoneyGiftGenerator implements GiftGeneratorInterface
{

    public function generate(): UserGift
    {
        return new MoneyGift();
    }
}