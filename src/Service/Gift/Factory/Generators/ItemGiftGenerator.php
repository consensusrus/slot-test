<?php

namespace App\Service\Gift\Factory\Generators;

use App\Entity\Gift\ItemGift;
use App\Entity\Gift\UserGift;

class ItemGiftGenerator implements GiftGeneratorInterface
{

    public function generate(): UserGift
    {
        return new ItemGift();
    }
}