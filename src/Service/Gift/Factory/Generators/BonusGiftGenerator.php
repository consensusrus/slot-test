<?php

namespace App\Service\Gift\Factory\Generators;

use App\Entity\Gift\BonusGift;
use App\Entity\Gift\UserGift;

class BonusGiftGenerator implements GiftGeneratorInterface
{

    public function generate(): UserGift
    {
        return new BonusGift();
    }
}