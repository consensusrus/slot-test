<?php

namespace App\Service\Gift\Factory\Generators;

use App\Entity\Gift\UserGift;

interface GiftGeneratorInterface
{

    public function generate(): UserGift;
}