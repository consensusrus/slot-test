<?php

namespace App\DataFixtures;

use App\Entity\GiftItem\GiftItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GiftItemsFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        foreach ([ 'Телевизор', 'Посудомойка', 'Плэйстэйшон 5' ] as $giftItemTitle) {
            $giftItem = new GiftItem();

            $giftItem->setTitle($giftItemTitle);

            $manager->persist($giftItem);
        }

        $manager->flush();
    }
}
