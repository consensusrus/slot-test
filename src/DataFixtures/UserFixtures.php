<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;


    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {

        $this->passwordEncoder = $passwordEncoder;
    }


    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();

        $userAdmin->setEmail('admin@admin.ru');
        $userAdmin->setRoles([ 'ROLE_ADMIN', 'ROLE_USER' ]);

        $userAdmin->setPassword($this->passwordEncoder->encodePassword(
            $userAdmin,
            'admin'
        ));

        $manager->persist($userAdmin);

        foreach (range(0, 500) as $userIndex) {
            $user = new User();

            $user->setEmail(sprintf('user%s@user.ru', $userIndex));
            $user->setRoles([ 'ROLE_USER' ]);

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'user'
            ));

            $manager->persist($user);
            $manager->flush();
        }
    }
}
