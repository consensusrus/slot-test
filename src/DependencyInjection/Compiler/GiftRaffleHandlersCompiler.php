<?php

namespace App\DependencyInjection\Compiler;

use App\Service\Gift\GiftRaffleHandlersManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class GiftRaffleHandlersCompiler implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if ( ! $container->has(GiftRaffleHandlersManager::class)) {
            return;
        }

        $definition     = $container->findDefinition(GiftRaffleHandlersManager::class);
        $taggedServices = $container->findTaggedServiceIds('gifts.raffle_handlers');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addHandler', [ new Reference($id) ]);
        }
    }
}