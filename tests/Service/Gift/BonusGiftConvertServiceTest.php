<?php

namespace App\Tests\Service\Gift;

use App\Entity\Gift\BonusGift;
use App\Service\Gift\BonusGiftConvertService;
use App\Service\Gift\Factory\GiftFactory;
use PHPUnit\Framework\TestCase;

class BonusGiftConvertServiceTest extends TestCase
{

    public function testConvertMoneyToBonus()
    {
        $calculator = new BonusGiftConvertService();

        $moneyGift = GiftFactory::makeGift(GiftFactory::GIFT_MONEY);

        $moneyGift
            ->getValue()
            ->setValue(123);

        $bonusGift = $calculator->convertMoneyToBonus($moneyGift);

        $this->assertInstanceOf(BonusGift::class, $bonusGift);

        $this->assertEquals(270, $bonusGift->getTextValue());
    }
}
