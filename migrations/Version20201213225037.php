<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201213225037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE gifts (id INT NOT NULL, user_id INT DEFAULT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_651BCF2FA76ED395 ON gifts (user_id)');
        $this->addSql('CREATE TABLE gifts_bonus_values (id INT NOT NULL, gift_id INT DEFAULT NULL, value INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_23D1318497A95A83 ON gifts_bonus_values (gift_id)');
        $this->addSql('CREATE TABLE gifts_item_values (id INT NOT NULL, gift_item_id INT DEFAULT NULL, gift_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_73ADA91C7E3561F1 ON gifts_item_values (gift_item_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_73ADA91C97A95A83 ON gifts_item_values (gift_id)');
        $this->addSql('CREATE TABLE gifts_items (id INT NOT NULL, title TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE gifts_money_values (id INT NOT NULL, gift_id INT DEFAULT NULL, value DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6C45F04C97A95A83 ON gifts_money_values (gift_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE gifts ADD CONSTRAINT FK_651BCF2FA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE gifts_bonus_values ADD CONSTRAINT FK_23D1318497A95A83 FOREIGN KEY (gift_id) REFERENCES gifts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE gifts_item_values ADD CONSTRAINT FK_73ADA91C7E3561F1 FOREIGN KEY (gift_item_id) REFERENCES gifts_items (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE gifts_item_values ADD CONSTRAINT FK_73ADA91C97A95A83 FOREIGN KEY (gift_id) REFERENCES gifts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE gifts_money_values ADD CONSTRAINT FK_6C45F04C97A95A83 FOREIGN KEY (gift_id) REFERENCES gifts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE gifts_bonus_values DROP CONSTRAINT FK_23D1318497A95A83');
        $this->addSql('ALTER TABLE gifts_item_values DROP CONSTRAINT FK_73ADA91C97A95A83');
        $this->addSql('ALTER TABLE gifts_money_values DROP CONSTRAINT FK_6C45F04C97A95A83');
        $this->addSql('ALTER TABLE gifts_item_values DROP CONSTRAINT FK_73ADA91C7E3561F1');
        $this->addSql('ALTER TABLE gifts DROP CONSTRAINT FK_651BCF2FA76ED395');
        $this->addSql('DROP TABLE gifts');
        $this->addSql('DROP TABLE gifts_bonus_values');
        $this->addSql('DROP TABLE gifts_item_values');
        $this->addSql('DROP TABLE gifts_items');
        $this->addSql('DROP TABLE gifts_money_values');
        $this->addSql('DROP TABLE "user"');
    }
}
