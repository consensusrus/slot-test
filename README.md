1. Склонировать репо
2. Сделать composer install
3. Выполнить миграции php bin/console doctrine:migrations:migrate
4. Подгрузить фикстуры php bin/console doctrine:fixtures:load

Команда для распределения призов для пользователей (учитывается что пользователи без призов)

php bin/console gift:raffle N

где N - batch size.

Стандартные пользовательские доступы

admin@admin.ru | admin

userN@user.ru | user

где N - число от 0 до 500.

Запуск тестов:

./bin/phpunit